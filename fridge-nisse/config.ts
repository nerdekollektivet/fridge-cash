/// Private key for wallet. Set to null to generate a private key and exit.
/// If the key below starts with 'f816', it's the unsecure DEMO KEY!!
/// TODO: Switch to WIF encoding, rather than raw hex.
// export const PRIVATE_KEY = "f816ae0eedd568d802070d57bf8ae2aa0f1b77b615f3828942fe2322114a65be"
// export const PRIVATE_KEY = "3e5c78b733081daca02d94a73e854b1d32c0732b339caae0525c8e1fc7799421";
export const PRIVATE_KEY = null;

/// Set to null if refill purchases are priced in BCH
export const CONVERSION_CURRENCY = 'nok';

/// Number of slots before purchasing new refreshments.
export const PURCHASE_SLOTS = 24;

/// Amount of CONVERSION_CURRENCY units to purchase refillment.
// export const NEW_PURCHASE_THRESHOLD = PURCHASE_SLOTS * 35; // NOK
export const NEW_PURCHASE_THRESHOLD = 1.5;

/// The minimum amount to charge for a beverage (in case of bull market)
export const MINIMUM_UNIT_PRICE = (NEW_PURCHASE_THRESHOLD / PURCHASE_SLOTS) * 0.8; // NOK

/// The maximum amount to change for a bevarge (in case of bear market)
export const MAXIMUM_UNIT_PRICE = (NEW_PURCHASE_THRESHOLD / PURCHASE_SLOTS) * 1.3; // NOK

/// Where to send coins when purchasing refillment
/// pr7sf... is cashaccount nerdekollektivet#110310;
export const MERCHANT_DESTINATION_ADDRESS = 'bitcoincash:pr7sf5lm96e5d8gw6jf08ln4p0jfk609qqa0mjf6sw'

/// NYI! Where to send refillment order e-mail
export const MERCHANT_DESTINATION_EMAIL = 'merchant@example.org';

/// How often to update the fiat/BCH conversion rate (in seconds)
export const CONVERSION_UPDATE_INTERVAL = 600;

/// satoshis per BCH
export const COIN = 100000000;

export const IS_DEMO = true;
