const util = require('util')
const chalk = require('chalk');
const bitcore = require('bitcore-lib-cash');
const fortune = require('random-fortune');
const assert = require('assert');

import { getUtxos, getTransaction } from './electrum';
import { IS_DEMO } from './config';

const log = console.log;

async function findInputAddress(sourceTxid: string) {
    const tx = await getTransaction(sourceTxid, true /* verbose */) as any;
    const inputTx = await getTransaction(tx.vin[0].txid, true /* verbose */) as any;
    const scriptPubKey = inputTx.vout[tx.vin[0].vout].scriptPubKey
    if (!scriptPubKey.addresses || !scriptPubKey.addresses.length) {
        return null;
    }
    return scriptPubKey.addresses[0];
};

export async function createTransaction(
    amount: number,
    destination: string,
    spendFromAddress: string,
    privateKeyBuffer: Uint8Array): Promise<string>
{
    const utxos = await getUtxos(spendFromAddress) as any;
    const bn = bitcore.crypto.BN.fromBuffer(privateKeyBuffer);
    const privateKey = new bitcore.PrivateKey(bn);

    let inputAmount = 0;

    const spendFrom = bitcore.Address(spendFromAddress);

    const utxosBitcore = utxos.map((i) => {
        inputAmount += i.value;
        return {
            txId: i.tx_hash,
            outputIndex: i.tx_pos,
            address: spendFromAddress,
            script: new bitcore.Script(spendFrom).toHex(),
            satoshis: i.value,
        }
    });
    if (inputAmount < amount) {
        throw Error(`Not enough funds to create transaction (${inputAmount} < ${amount})`);
    }

    const tx = new bitcore.Transaction()
        .from(utxosBitcore)
        .addData("autonomous fridge.cash purchase!")
        .to(destination, amount)
        .change(spendFrom)
        .feePerKb(2000)
        .sign(privateKey);

    return tx.serialize();
};

export async function createThanksToken(
    sourceTxId: string,
    spendFromAddress: string,
    privateKeyBuffer: Uint8Array): Promise<string>
{
    const destination = await findInputAddress(sourceTxId);
    if (destination == null) {
        log(`Failed to find input address from transaction ${sourceTxId}`);
        return;
    }

    const bn = bitcore.crypto.BN.fromBuffer(privateKeyBuffer);
    const privateKey = new bitcore.PrivateKey(bn);
    const spendFrom = bitcore.Address(spendFromAddress);

    const utxos = await getUtxos(spendFromAddress) as any;
    const utxosBitcore = [];
    let inputAmount = 0;
    for (const i of utxos) {
        inputAmount += i.value;
        utxosBitcore.push({
            txId: i.tx_hash,
            outputIndex: i.tx_pos,
            address: spendFromAddress,
            script: new bitcore.Script(spendFrom).toHex(),
            satoshis: i.value,
        });
        if (inputAmount >= 5000) {
            // More than enough for SLP output + change output + fee.
            break;
        }
    };
    if (inputAmount < 5000) {
        throw Error(`${inputAmount} is not enough coins to send thanks`);
    }
    console.log("Input amount: ", inputAmount);
    const DUST_AMOUNT = 546;

    const OP_RETURN = Buffer.from([0x6a]);
    const OP_PUSH1 = Buffer.from([0x4c]);
    const slpPrefix = Buffer.from('534c5000', "hex");
    const tokenType = Buffer.from([0x01]);
    const action = Buffer.from('GENESIS');
    let ticker = Buffer.from("🧉FORTUNE");
    if (IS_DEMO) {
        ticker = Buffer.from("🐵FORTUNE-DEMO");
    }
    // Fortune length is limited by OP_RETURN datacarrier size, which is at
    // 220 bytes. We remove 35 bytes for overhead of creating the SLP token.
    const MAX_FORTUNE_LENGTH = 188;
    let fortuneStr = "";
    do {
        fortuneStr = fortune.fortune(MAX_FORTUNE_LENGTH);
    } while (fortuneStr.length > MAX_FORTUNE_LENGTH);
    const tokenName = Buffer.from(fortuneStr);
    assert(tokenName.length);
    const documentURI = Buffer.from("https://fridge.cash");

    // this is an invalid vout, which is the same as no minting baton.
    const mintingbatonVout = Buffer.from([420]);

    const payload = Buffer.concat([
        OP_RETURN,

        Buffer.from([slpPrefix.length]),
        slpPrefix,

        Buffer.from([tokenType.length]),
        tokenType,

        Buffer.from([action.length]),
        action,

        Buffer.from([ticker.length]),
        ticker,

        OP_PUSH1, // Name can be very long, so we need OP_PUSH1
        Buffer.from([tokenName.length]),
        tokenName,

        Buffer.from([documentURI.length]),
        documentURI,

        // No document hash (we need the space for the fortune)
        OP_PUSH1,
        Buffer.from([0]),

        // OP_PUSH1,
        Buffer.from([1]), /* length of push */
        Buffer.from([0]), /* decimals */

        // OP_PUSH1,
        Buffer.from([mintingbatonVout.length]),
        Buffer.from(mintingbatonVout),

        OP_PUSH1,
        Buffer.from([8]), // initial mint quantity length
        Buffer.from('0000000000000001', 'hex'), // initial mint quantity
    ]);

    const tx = new bitcore.Transaction()
        .from(utxosBitcore)
        .addOutput(bitcore.Transaction.Output({
            script: payload,
            satoshis: 0,
        }))
        .to(destination, DUST_AMOUNT)
        .change(spendFrom)
        .feePerKb(2000)
        .sign(privateKey);

    const serialized = tx.serialize({
        'disableDustOutputs': false,
    });
    console.log(serialized);
    return serialized;
};
